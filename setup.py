import os, sys
import time

from tqdm import tqdm

i = 1

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def main():
    try:
        user = os.environ.__getitem__('LOGNAME')
        print(f'{bcolors.BOLD}CocoyoyoLibrairie setup.py:')
        print(f'{bcolors.OKGREEN}Starting...\n')
        for i in tqdm(range(100), colour='GREEN', desc=f'{bcolors.OKBLUE}Starting the setup please wait '):
            time.sleep(0.1)
        for i in tqdm(range(100), colour='GREEN', desc=f'{bcolors.OKBLUE}Get UserName '):
            time.sleep(0.01)
        print(f'\n{bcolors.HEADER}\t-' + user + '\n')
        print(f'{bcolors.OKGREEN}Check ✅\n')
        time.sleep(0.5)
        for i in tqdm(range(100), colour='GREEN', desc=f'{bcolors.OKBLUE}Get path to Documents and Desktop '):
            time.sleep(0.01)
        path = fr'C:/Users/{user}/Documents'
        desktop_path = fr'C:/Users/{user}/Desktop'
        print(f'\n{bcolors.HEADER}\t-' + path + ' | Document \n')
        print(f'\n{bcolors.HEADER}\t-' + desktop_path + ' | Desktop \n')
        print(f'{bcolors.OKGREEN}Check ✅\n')
        time.sleep(0.5)
        with open('app.py') as have_path:
            current_path = os.path.realpath(have_path.name)
            current_path, y = current_path.split('/app.p')
            for i in tqdm(range(100), colour='GREEN', desc=f'{bcolors.OKBLUE}Get path to the app directory '):
                time.sleep(0.01)
            print(f'\n{bcolors.HEADER}\t-' + current_path + '\n')
            print(f'{bcolors.OKGREEN}Check ✅\n')
            time.sleep(0.5)
            have_path.close()
        with open('setup.bat', 'w+') as setup:
            for i in tqdm(range(100), colour='GREEN', desc=f'{bcolors.OKBLUE}Write file setup.bat '):
                time.sleep(0.01)
            content = f"""
@echo off
cd {path}
mkdir cocoyoyolibrairie_search_app
cd cocoyoyolibrairie_search_app
mv {current_path} {path}
rm {path}
fsutil hardlink create {desktop_path}/Cocoyoyolibrairie.py app.py
            """
            setup.write(content)
            setup.close()
            print(f'\n{bcolors.HEADER}\t-' + setup.name + '\n')
            print(f'{bcolors.OKGREEN}Check ✅\n')
            time.sleep(0.5)
            for i in tqdm(range(100), colour='GREEN', desc=f'{bcolors.OKBLUE}Execute file setup.bat '):
                time.sleep(0.01)
            os.system('setup.bat')
            print(f'\n{bcolors.HEADER}\t-' + 'Success' + '\n')
            print(f'{bcolors.OKGREEN}Check ✅\n')
            time.sleep(0.5)
        print(f'{bcolors.WARNING}\n\nSetup is finish you can start the .ink document on your Desktop !')
    except sys.exc_info() as e:
        print(e)
        print(f'{bcolors.FAIL}Failed to setup the app, please restart...')


if __name__ == '__main__':
    from colorama import Fore
    colors = Fore.__dict__
    colors.pop('RESET')
    main()
