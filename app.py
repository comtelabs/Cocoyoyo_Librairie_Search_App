import json
import runpy
from pprint import pprint
from tkinter import *
from tkinter import messagebox, filedialog
import webbrowser, os, requests, pyperclip
from link import link as l
from json import loads

with open('settings.json', 'r+') as s:
    s = loads(s.read())
    resolution = s['resolution']
    if s['theme'] == 'black':
        theme = 'black'
        theme2 = 'white'
    if s['bg-img'] != 'null':
        img = s['bg-img']
        if s['theme'] == 'black':
            theme = 'black'
            theme2 = 'white'
        else:
            theme = 'white'
            theme2 = 'black'
    else:
        theme = 'white'
        theme2 = 'black'
        if s['theme'] == 'black':
            theme = 'black'
            theme2 = 'white'
        if s['theme'] == 'white':
            theme = 'white'
            theme2 = 'black'
        with open('settings.json') as nothing:
            if s['bg-img'] != 'null':
                img = s['bg-img']
            else:
                img = theme
            nothing.close()


def search():
    books_l = {}
    books_d = {}
    books_a = {}
    books_t = {}
    canvas.destroy()
    right_frame.grid(row=0, column=0, sticky=N)
    something = input.get()
    if len(something) == 0:
        something = " "
    title = Label(right_frame, text=f"Résultats de la recherche \"{something}\"", font=("Courrier", 25), bg=theme, fg=theme2)
    title.pack()
    json = loads(requests.get(f'https://cocoyoyo-librairie.camponovo.space/api/search:{something}').content)
    resultat_livres = json['resultat_livres']
    resultat_date = json['resultat_date']
    resultat_auteurs = json['resultat_auteurs']
    resultat_tags = json['resultat_tags']
    title = Label(right_frame, text="Par titre:", font=("Courrier", 20), bg=theme, fg=theme2)
    title.pack()
    for result in resultat_livres:
        books_l[result['titre']] = l(result['titre'], result['id'])
        def link(event):
            titre = event.widget.cget("text")
            id = books_l[f'{titre}'].id()
            open_page(id)
        title = Label(right_frame, text=f"{result['titre']}", font=("Courrier", 15), bg=theme, fg=theme2, cursor="hand2")
        title.pack()
        title.bind("<Button-1>", link)
    title = Label(right_frame, text="Par auteur:", font=("Courrier", 20), bg=theme, fg=theme2)
    title.pack()
    for result in resultat_auteurs:
        books_a[result['titre']] = l(result['titre'], result['id'])
        def link(event):
            titre = event.widget.cget("text")
            id = books_a[f'{titre}'].id()
            open_page(id)
        title = Label(right_frame, text=f"{result['titre']}", font=("Courrier", 15), bg=theme, fg=theme2, cursor="hand2")
        title.pack()
        title.bind("<Button-1>", link)
    title = Label(right_frame, text="Par tags:", font=("Courrier", 20), bg=theme, fg=theme2)
    title.pack()
    for result in resultat_tags:
        books_t[result['titre']] = l(result['titre'], result['id'])
        def link(event):
            titre = event.widget.cget("text")
            id = books_t[f'{titre}'].id()
            open_page(id)
        title = Label(right_frame, text=f"{result['titre']}", font=("Courrier", 15), bg=theme, fg=theme2, cursor="hand2")
        title.pack()
        title.bind("<Button-1>", link)
    title = Label(right_frame, text="Par date:", font=("Courrier", 20), bg=theme, fg=theme2)
    title.pack()
    for result in resultat_date:
        books_d[result['titre']] = l(result['titre'], result['id'])
        def link(event):
            titre = event.widget.cget("text")
            id = books_d[f'{titre}'].id()
            open_page(id)
        title = Label(right_frame, text=f"{result['titre']}", font=("Courrier", 15), bg=theme, fg=theme2, cursor="hand2")
        title.pack()
        title.bind("<Button-1>", link)
    button.destroy()
    input.destroy()
    titre.destroy()


def copy():
    input_getting = input.get()
    pyperclip.copy(input_getting)


def open_page(bid):
    webbrowser.open_new(f"https://cocoyoyo-librairie.camponovo.space/book/{bid}")


def site():
    webbrowser.open_new("https://cocoyoyo-librairie.camponovo.space")


def settings():
    window_settings = Tk()
    window_settings.title("Cocoyoyolibrairie App | Réglages")
    window_settings.geometry("1000x700")
    if os.environ.__getitem__('SHELL') == '/bin/bash':
        window_settings.iconbitmap("@asset/logo_only.xbm")
    else:
        window_settings.iconbitmap("asset/logo_only.ico")
    window_settings.config(background=theme)

    def own_resolution():
        resolution = input_set.get()
        try:
            x, y = resolution.split("x")
            with open('settings.json', 'r+') as js:
                json_ = loads(js.read())
                with open('settings.json', 'w+') as s:
                    file = {
                        "theme": f"{json_['theme']}",
                        "resolution": f"{resolution}",
                        "bg-img": f"{json_['bg-img']}"
                    }
                    s.write(json.dumps((file)))
                    s.close()
                    js.close()
            messagebox.showinfo("Cocoyoyolibrairie App | Réglages enregistrés", "Vos réglages ont bien été enregistrés !\nRedémmarez la fenêtre depuis le menu ou en faisant ctrl + E pour que ceux ci s'appliquent.")
            window_settings.destroy()
        except:
            messagebox.showerror('Cocoyoyolibrairie App | Erreur dans la résolution', 'Vous avez rentré une résolution non-conforme,\nVeuillez recommencez...')


    def set_settings():
        setting = listbox.curselection()
        setting_ = listbox_.curselection()
        if setting != () and setting != (0,) and setting != (1,) and setting_ != () and setting_ != (0,) and setting_ != (1,) and setting_ != (2,):
            with open('settings.json', 'r+') as s:
                st = loads(s.read())
                setting = st['theme']
                setting_ = st['resolution']
                s.close()
        if setting_ == ():
            with open('settings.json', 'r+') as s:
                st = loads(s.read())
                setting_ = st['resolution']
                s.close()
                if setting == (0,):
                    setting = "white"
                if setting == (1,):
                    setting = "black"
                if setting == ():
                    with open('settings.json', 'r+') as s:
                        st = loads(s.read())
                        setting = st['theme']
                        s.close()
        if setting == ():
            with open('settings.json', 'r+') as s:
                st = loads(s.read())
                setting = st['theme']
                s.close()
            if setting_ == (1,):
                width = window.winfo_screenwidth() / 2
                heigth = window.winfo_screenheight() / 2
                setting_ = f"{int(width)}x{int(heigth)}"
            if setting_ == (2,):
                setting_ = f"{window.winfo_screenwidth()}x{window.winfo_screenheight()}"
            if setting_ == (0,):
                width = window.winfo_screenwidth() / 3.5
                heigth = window.winfo_screenheight() / 3.5
                setting_ = f"{int(width)}x{int(heigth)}"
            if setting_ == ():
                with open('settings.json', 'r+') as s:
                    st = loads(s.read())
                    setting_ = st['resolution']
                    s.close()
        with open('settings.json', 'r+') as js:
            json_ = loads(js.read())
            with open('settings.json', 'w+') as s:
                file = {
                    "theme": f"{setting}",
                    "resolution": f"{setting_}",
                    "bg-img": f"{json_['bg-img']}"
                }
                s.write(json.dumps((file)))
                s.close()
                js.close()
        messagebox.showinfo("Cocoyoyolibrairie App | Réglages enregistrés", "Vos réglages ont bien été enregistrés !\nRedémmarez la fenêtre depuis le menu pour que ceux ci s'appliquent.")
        window_settings.destroy()

    def upload():
        ftypes = [('Images Files', '*.png *.PNG *.gif *.GIF *.pgm *.PGM *.ppm *.PPM'), ('All files', '*')]
        dlg = filedialog.Open(filetypes=ftypes)
        fl = dlg.show()
        with open(fl, 'r+') as a:
            with open('settings.json', 'r+') as js:
                json_ = loads(js.read())
                with open('settings.json', 'w+') as s:
                    file = {
                            "theme": f"{json_['theme']}",
                            "resolution": f"{json_['resolution']}",
                            "bg-img": f"{os.path.realpath(a.name)}"
                        }
                    s.write(json.dumps((file)))
                    s.close()
                    js.close()
                    a.close()
        messagebox.showinfo("Cocoyoyolibrairie App | Réglages enregistrés", "Vos réglages ont bien été enregistrés !\nRedémmarez la fenêtre depuis le menu pour que ceux ci s'appliquent.")
        window_settings.destroy()

    def img_reset():
        with open('settings.json', 'r+') as js:
            json_ = loads(js.read())
            with open('settings.json', 'w+') as s:
                file = {
                    "theme": f"{json_['theme']}",
                    "resolution": f"{json_['resolution']}",
                    "bg-img": "null"
                }
                s.write(json.dumps((file)))
                s.close()
                js.close()
        messagebox.showinfo("Cocoyoyolibrairie App | Réglages enregistrés", "Vos réglages ont bien été enregistrés !\nRedémmarez la fenêtre depuis le menu pour que ceux ci s'appliquent.")
        window_settings.destroy()

    titre_set = Label(window_settings, text="Séléctionnez le thème: (il faudra redémmarer l'app)", font=("Courrier", 25), bg=theme, fg=theme2)
    titre_set.pack()

    listbox = Listbox(window_settings, font=("Courrier", 10), bg=theme, fg=theme2, height=2)
    listbox.insert(0, 'Theme Blanc')
    listbox.insert(1, 'Theme Noir')
    listbox.pack(fill=X)

    btn_set = Button(window_settings, text="Séléctionner", font=("Courrier", 10), bg=theme, fg=theme2, command=set_settings)
    btn_set.pack(fill=X)

    space = Label(window_settings, text="\n\n", font=("Courrier", 10), bg=theme, fg=theme2)
    space.pack()

    titre_set = Label(window_settings, text="Séléctionnez la taille de l'app: (il faudra redémmarer l'app)", font=("Courrier", 25), bg=theme, fg=theme2)
    titre_set.pack()

    listbox_ = Listbox(window_settings, font=("Courrier", 10), bg=theme, fg=theme2, height=3)
    listbox_.insert(0, 'Petite')
    listbox_.insert(1, 'Moyenne')
    listbox_.insert(2, 'Plein écran')
    listbox_.pack(fill=X)

    btn_set_ = Button(window_settings, text="Séléctionner", font=("Courrier", 10), bg=theme, fg=theme2, command=set_settings)
    btn_set_.pack(fill=X)

    space = Label(window_settings, text="\n", font=("Courrier", 10), bg=theme, fg=theme2)
    space.pack()

    titre_set_ = Label(window_settings, text="Ou entrez votre résolution: (ex:500x450)", font=("Courrier", 20), bg=theme, fg=theme2)
    titre_set_.pack()

    space = Label(window_settings, text="\n", font=("Courrier", 10), bg=theme, fg=theme2)
    space.pack()

    input_set = Entry(window_settings, font=("Courrier", 10), bg=theme, fg=theme2)
    input_set.pack()

    space = Label(window_settings, text="\n", font=("Courrier", 10), bg=theme, fg=theme2)
    space.pack()

    btn_set_ = Button(window_settings, text="Valider ma résolution", font=("Courrier", 10), bg=theme, fg=theme2, command=own_resolution)
    btn_set_.pack(fill=X)

    space = Label(window_settings, text="\n\n", font=("Courrier", 10), bg=theme, fg=theme2)
    space.pack()

    titre_set_img = Label(window_settings, text="Image de fond", font=("Courrier", 25), bg=theme, fg=theme2)
    titre_set_img.pack()

    btn_set_img = Button(window_settings, text="Uploader un fichier", font=("Courrier", 10), bg=theme, fg=theme2, command=upload)
    btn_set_img.pack(fill=X)

    space = Label(window_settings, text="\n", font=("Courrier", 2), bg=theme, fg=theme2)
    space.pack()

    btn_set_reset = Button(window_settings, text="Réinitialiser l'image de fond", font=("Courrier", 10), bg=theme, fg=theme2, command=img_reset)
    btn_set_reset.pack(fill=X)

    window_settings.mainloop()


def restart():
    window.destroy()
    runpy.run_path('app.py')


def web():
    webbrowser.open_new('https://gitlab.com/comtelabs/Cocoyoyo_Librairie_Search_App/-/blob/master/README.md')


window = Tk()
window.title("Cocoyoyolibrairie App | Rechercher des livres")
window.geometry(resolution)
window.bind("<Control-e>", lambda x: restart())
window.bind("<Control-r>", lambda x: restart())
window.bind("<Control-q>", lambda x: exit())
window.bind("<Control-s>", lambda x: settings())
window.bind("<Control-f>", lambda x: search())
if os.environ.__getitem__('SHELL') == '/bin/bash':
    window.iconbitmap("@asset/logo_only.xbm")
else:
    window.iconbitmap("asset/logo_only.ico")

if img != 'black' and img != 'white':
    background_image = PhotoImage(file=img)
    background_label = Label(window, image=background_image)
    background_label.place(x=0, y=0, relwidth=1, relheight=1)
    background_label.image = background_image

if img == 'black' or img == 'white':
    window.config(background=img)

frame = Frame(window, bg=theme)

if img != 'black' and img != 'white':
    background_image = PhotoImage(file=img).zoom(1).subsample(1)
    background_label = Label(frame, image=background_image)
    background_label.place(x=-15, y=-100, relwidth=1, relheight=1)
    background_label.image = background_image

if img == 'black' or img == 'white':
    window.config(background=img)

width = 312
height = 312
image = PhotoImage(file="asset/logo_only.png").zoom(35).subsample(35)
if img != 'black' and img != 'white':
    canvas = Canvas(frame, width=width, height=height, bg=None, bd=0, highlightthickness=0)
if img == 'black' or img == 'white':
    canvas = Canvas(frame, width=width, height=height, bg=theme, bd=0, highlightthickness=0)

if img != 'black' and img != 'white':
    canvas.grid(row=0, column=0, sticky=W)

if img == 'black' or img == 'white':
    canvas.create_image(width/2, height/2, image=image)
    canvas.grid(row=0, column=0, sticky=W)

right_frame = Frame(frame, bg=theme)

if img != 'black' and img != 'white':
    canvas.destroy()

if img == 'black' or img == 'white':
    pass

titre = Label(right_frame, text="Rechercher sur la cocoyoyo-librairie", font=("Courrier", 25), bg=theme, fg=theme2)
input = Entry(right_frame, font=("Courrier", 25), bg=theme, fg=theme2)
button = Button(right_frame, text="Rechercher", font=("Courrier", 22), bg=theme, fg=theme2, command=search, cursor="fleur")
titre.pack()
input.pack(fill=X)
button.pack(fill=X)

right_frame.grid(row=0, column=1, sticky=W)

frame.pack(expand=YES)

menu = Menu(window)
file_menu = Menu(menu, tearoff=5)
file_menu.add_command(label="Rechercher (ctrl + F)", command=search)
file_menu.add_command(label="Voir le site", command=site)
menu.add_cascade(label="Général", menu=file_menu)
quit_menu = Menu(menu, tearoff=5)
quit_menu.add_command(label="Quitter (ctrl + Q)", command=exit)
quit_menu.add_command(label="Redémarrer (ctrl + E)", command=restart)
menu.add_cascade(label="Fenêtre", menu=quit_menu)
reset_menu = Menu(menu, tearoff=5)
reset_menu.add_command(label="Réinitialiser (ctrl + R)", command=restart)
menu.add_cascade(label="Réinitialiser la recherche", menu=reset_menu)
settings_menu = Menu(menu, tearoff=5)
settings_menu.add_command(label="Réglages (ctrl + S)", command=settings)
menu.add_cascade(label="Réglages", menu=settings_menu)
other_menu = Menu(menu, tearoff=5)
other_menu.add_command(label="Tutoriel", command=web)
menu.add_cascade(label="Autres", menu=other_menu)

window.config(menu=menu)

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


text = f"""   
{bcolors.OKBLUE}   ______      _      ____    ____      _      _______    ____    ____ {bcolors.ENDC}        {bcolors.FAIL} ______  ________ ____   ____  
{bcolors.OKBLUE} .' ___  |    / \    |_   \  /   _|    / \    |_   __ \  |_   \  /   _|{bcolors.ENDC}        {bcolors.FAIL}|_   _ `|_   __  |_  _| |_  _| 
{bcolors.OKBLUE}/ .'   \_|   / _ \     |   \/   |     / _ \     | |__) |   |   \/   |  {bcolors.ENDC} ______ {bcolors.FAIL}  | | `. \| |_ \_| \ \   / /   
{bcolors.OKBLUE}| |         / ___ \    | |\  /| |    / ___ \    |  __ /    | |\  /| |  {bcolors.ENDC} ______ {bcolors.FAIL}  | |  | ||  _| _   \ \ / /    
{bcolors.OKBLUE}\\ `.___.'\_/ /   \ \_ _| |_\/_| |_ _/ /   \ \_ _| |  \ \_ _| |_\/_| |_{bcolors.ENDC}        {bcolors.FAIL}  _| |_.' _| |__/ |   \ ' /     
{bcolors.OKBLUE} `.____ .|____| |____|_____||_____|____| |____|____| |___|_____||_____|{bcolors.ENDC}        {bcolors.FAIL}|______.|________|    \_/    
 """

print(text)

print(bcolors.FAIL, "\nDon\'t touch me please\nI am here for you, if you kill me, you kill the application too !\nQuit the application with the menu or ctrl + Q.")

window.mainloop()
